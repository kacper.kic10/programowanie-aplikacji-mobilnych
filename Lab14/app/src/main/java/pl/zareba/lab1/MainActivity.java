package pl.zareba.lab1;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String USER_NAME = "USER_NAME";
    private Object view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button logButton = (Button) findViewById(R.id.log_button);
        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.message_edittext);
                String text = editText.getText().toString();
                android.util.Log.d("My Application",text);
            }
        });

        Button loginButton = (Button)findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText password = (EditText)findViewById(R.id.password_edittext);
                EditText username = (EditText)findViewById(R.id.login_edittext);
                if(username.getText().toString().equals("Kacper")&&password.getText().toString().equals("androidMaster")) {
                    Intent intent = new Intent(getApplicationContext(), Logcomplete.class);
                    String message = username.getText().toString();
                    intent.putExtra(USER_NAME, message);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Niepoprawny login lub hasło", Toast.LENGTH_SHORT).show();
                }
            }
        });




        final Button rewriteButton = (Button)findViewById(R.id.rewrite_button);
        rewriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.message_edittext);
                String text = editText.getText().toString();
                TextView textView = (TextView)findViewById(R.id.rewrite_button);
                textView.setText(text);
            }
        });


    }
    public void onLogClick(View view) {
        Context context = getApplicationContext();
        EditText editText = (EditText) findViewById(R.id.message_edittext);
        String text = editText.getText().toString();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context,text,duration);
        toast.show();
    }

}

