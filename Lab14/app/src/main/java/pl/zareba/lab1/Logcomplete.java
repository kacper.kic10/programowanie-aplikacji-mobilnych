package pl.zareba.lab1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Logcomplete extends AppCompatActivity {
TextView hello;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logcomplete);

        hello=findViewById(R.id.logged);
        Intent intent = getIntent();
        String str = intent.getStringExtra(MainActivity.USER_NAME);
        hello.setText("Witaj " + str);
    }
}
