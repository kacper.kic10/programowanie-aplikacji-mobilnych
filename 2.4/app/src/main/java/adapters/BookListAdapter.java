package adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import models.Book;
import pl.zareba.bookweb.Category;
import pl.zareba.bookweb.R;


public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder> {
    private List<Book> books;
    private RecyclerView mRecyclerView;
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImageView;
        public TextView title;
        public TextView author;
        public TextView date;
        public TextView category;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView=itemView.findViewById(R.id.ksiazka);
            title =itemView.findViewById(R.id.title);
            author =itemView.findViewById(R.id.author);
            date=itemView.findViewById(R.id.date);
            category=itemView.findViewById(R.id.category);

        }
    }
    public BookListAdapter(List<Book> arrayList, RecyclerView pRecyclerView ){
        books = arrayList;
        mRecyclerView = pRecyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View sciezka=LayoutInflater.from(parent.getContext()).inflate(R.layout.books_list_element, parent, false);
        ViewHolder wyglad = new ViewHolder(sciezka);
        return wyglad;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Book currentItem=books.get(position);
        holder.title.setText(currentItem.getTitle());
        holder.author.setText(currentItem.getAuthor());
        holder.date.setText(currentItem.getDate());
        holder.category.setText(currentItem.getCategory());
    }

    @Override
    public int getItemCount() {

        return books.size();
    }
}
