package models;

import pl.zareba.bookweb.Category;

public class Book {

    private String title;
    private String author;
    private String date;
    private Category category;


    public Book(String title, String author, String date, Category category) {
        this.title = title;
        this.author = author;
        this.date = date;
        this.category = category;

    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public CharSequence getCategory() {
        return category;
    }

    public String getDate() {

        return date;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", date=" + date + '\'' +
                ", date=" + category +
                '}';
    }
}
