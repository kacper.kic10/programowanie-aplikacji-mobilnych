package pl.zareba.bookweb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class AddCommentActivity extends AppCompatActivity {
    public EditText komentarz;
    public Button dodajkomentarz;
    String tekst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);
        komentarz = (EditText)findViewById(R.id.komentarz);
        dodajkomentarz = (Button) findViewById(R.id.dodajkomentarz);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dodajkomentarz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tekst= komentarz.getText().toString().trim();
                if (komentarz.getText().toString().trim().equalsIgnoreCase("")){
                    komentarz.setError("Wpisz komentarz");
                }
                else{
                    Save(tekst);
                }
            }
        });
    }
    private void Save(String Text) {

        try {
            File fileName=new File(getFilesDir(), "komentarze.txt");
            File file = (fileName);
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(Text +'\n');
            bw.close();
            Toast.makeText(AddCommentActivity.this, fileName + " zapisano ", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Toast.makeText(AddCommentActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
