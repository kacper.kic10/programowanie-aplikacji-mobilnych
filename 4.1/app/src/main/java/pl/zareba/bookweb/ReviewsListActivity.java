package pl.zareba.bookweb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import adapters.CommentsListAdapter;
import models.Kom;

public class ReviewsListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews_list);
        RecyclerView recyclerView = findViewById(R.id.review_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            File fileName=new File(getFilesDir(), "recenzje.txt");
            List<Kom> arrayList = new ArrayList<>();
            Scanner s = new Scanner(fileName);
            while (s.hasNextLine()){
                arrayList.add(new Kom(s.nextLine()));
                recyclerView.setAdapter(new CommentsListAdapter(arrayList, recyclerView));
            }
            s.close();
        }
        catch (Exception e) {
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}