package pl.zareba.bookweb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class AddReviewActivity extends AppCompatActivity {
    public EditText recenzja;
    public Button dodajrecenzje;
    String tekstr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review);
        recenzja = (EditText)findViewById(R.id.dodrecenzja);
        dodajrecenzje = (Button) findViewById(R.id.dodajrecenzje);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dodajrecenzje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tekstr= recenzja.getText().toString().trim();
                if (recenzja.getText().toString().trim().equalsIgnoreCase("")){
                    recenzja.setError("Wpisz recenzję");
                }
                else{
                    Save(tekstr);
                }
            }
        });
    }
    private void Save(String Text) {

        try {
            File fileName=new File(getFilesDir(), "recenzje.txt");
            File file = (fileName);
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(Text +'\n');
            bw.close();
            Toast.makeText(AddReviewActivity.this, fileName + " zapisano ", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Toast.makeText(AddReviewActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
