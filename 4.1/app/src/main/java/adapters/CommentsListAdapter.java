package adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pl.zareba.bookweb.R;
import models.Kom;

public class CommentsListAdapter extends RecyclerView.Adapter<CommentsListAdapter.ViewHolder> {
    private List<Kom> wypelnienie;
    private RecyclerView mRecyclerView;
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView komentarz;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            komentarz=itemView.findViewById(R.id.komentarz);
        }
    }
    public CommentsListAdapter(List<Kom> arrayList, RecyclerView pRecyclerView ){
        wypelnienie = arrayList;
        mRecyclerView = pRecyclerView;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View sciezka= LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_set, parent, false);
        ViewHolder wyglad = new ViewHolder(sciezka);
        return wyglad;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Kom currentItem= wypelnienie.get(position);
        holder.komentarz.setText(currentItem.getKomentarz());
    }
    @Override
    public int getItemCount() {
        return wypelnienie.size();
    }
}
