package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private Button log_button;
    private EditText message_edittext;
    private Button toast_button;
    private Button rewrite_button;
    private TextView rewrited_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log_button=findViewById(R.id. log_button);
        message_edittext=findViewById((R.id. message_edittext));
        toast_button=findViewById(R.id.toast_button);
        rewrite_button=findViewById(R.id.rewrite_button);
        rewrited_button=findViewById((R.id.rewrited_button));

    }
    public void logTo (View view) {
        EditText editText = (EditText) findViewById(R.id.message_edittext);
        String text = editText.getText().toString();
        android.util.Log.d("My Application",text);
    };


    public void rewriteMess(View view) {
        if (message_edittext.getText().toString().isEmpty()) {
            rewrited_button.setText("Wpisz tekst");
        } else {
            rewrited_button.setText(message_edittext.getText().toString());
        }
    }
    public void toastButton(View view) {
    Toast.makeText(getApplicationContext(),
            " " + message_edittext.getText(),
            Toast.LENGTH_SHORT).show();
    }
}
