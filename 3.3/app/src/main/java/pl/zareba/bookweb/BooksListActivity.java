package pl.zareba.bookweb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import adapters.BookListAdapter;
import models.Book;


public class BooksListActivity extends AppCompatActivity {
    ListView books;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_list);
        RecyclerView recyclerView = findViewById(R.id.books_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        List<Book> arrayList= new ArrayList<>();

        arrayList.add(new Book("Android Studio. Tworzenie aplikacji mobilnych","Marcin Płonkowski","2015.07.15", Category.Informatyka));
        arrayList.add(new Book("Elektronika - ależ to bardzo proste","Andrzej Dobrowolski, Zbigniew Jachna, Ewelina Majda, Mariusz Wierzbowski","2014.04.15", Category.Elektornika));
        arrayList.add(new Book("Matematyka od zera dla inżyniera","Dexter J. Booth, K.A. Stroud","2017.06.17",Category.Matematyka));
        arrayList.add(new Book("Tytuł4","Autor4","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł5","Autor5","2020.03.16",Category.Przyrodnicze));
        arrayList.add(new Book("Tytuł6","Autor6","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł7","Autor7","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł8","Autor8","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł9","Autor9","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł10","Autor10","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł11","Autor11","2020.03.16",Category.Scienefiction));
        arrayList.add(new Book("Tytuł12","Autor12","2020.03.16",Category.Scienefiction));


        recyclerView.setAdapter(new BookListAdapter(arrayList, recyclerView));



        for (Book print:arrayList) {
            Log.d("książka", String.valueOf(print));

        }
    }

}
