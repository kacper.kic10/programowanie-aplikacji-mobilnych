package pl.zareba.bookweb;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    public static SharedPreferences myPreferences;

    public static final String MY_NAME = "MyPrefsFile";

    public static EditText mNameEt;
    public static String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = getSharedPreferences(MY_NAME, MODE_PRIVATE);
        name = prefs.getString("name", null);

        if (name != null  )
        {

            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
        }


        Button gobutton = findViewById(R.id.button);

        mNameEt=(EditText)findViewById(R.id.imie);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        myPreferences= getSharedPreferences("name", MODE_PRIVATE);
        gobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sprawdz(mNameEt.getText().toString());
            }
        });
    }

    public void sprawdz(String imie){
        if((imie.isEmpty() )){

            Toast.makeText(MainActivity.this, "Puste pole! Proszę wpisać imie!", Toast.LENGTH_SHORT).show();

        }else{

            name = mNameEt.getText().toString();
            SharedPreferences.Editor editor = getSharedPreferences(MY_NAME, MODE_PRIVATE).edit();
            editor.putString("name", name);
            editor.apply();
            SharedPreferences prefs = getSharedPreferences(MY_NAME, MODE_PRIVATE);
            name = prefs.getString("name", null);
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.puste_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.about) {
            Intent intent = new Intent(MainActivity.this, AboutProgramActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
